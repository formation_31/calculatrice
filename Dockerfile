FROM python:3.10-slim-buster
ENV PYTHONUNBUFFERED 1

# https://pythonspeed.com/articles/activate-virtualenv-dockerfile/

#ENV VIRTUAL_ENV=/opt/venv
#RUN python3 -m venv $VIRTUAL_ENV
#ENV PATH="$VIRTUAL_ENV/bin:$PATH"

RUN mkdir /app
RUN pip install --upgrade pip

ADD requirements.txt /app/
COPY src /app/

WORKDIR /app
RUN pip install -r /app/requirements.txt

CMD [ "python", "-u", "calculatrice/calcule.py" ]

