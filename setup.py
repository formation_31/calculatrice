# -*- coding: utf-8 -*-
import os

from setuptools import find_packages, setup

setup(
    name='calculatrice',
    version='1.0',
    description="Python demo code",
    long_description="Python demo code",
    author='Scalian',
    package_dir={"":"src"},
    packages=find_packages(where="src", exclude=("tests",)),
    url="https://gitlab.cnes.fr/sandbox_qualite/calculatrice", 
    classifiers=[ # Liste des classifiers (utilité limité si distribué seulement via artifactory) : https://pypi.org/classifiers/
    "Programming Language :: Python :: 3",
    "License :: OSI Approved :: MIT License",
    "Operating System :: OS Independent",
    ],
    python_requires='>=3.7'

)
