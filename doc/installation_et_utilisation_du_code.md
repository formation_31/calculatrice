## Utilisation sous linux

### Pre-requis

il faut:
1. l'interpréteur Python, si possible après la version 3.7.
3. git
4. un terminal  
3. recommandé: une IDE, par exemple Visual Studio Code (VS code) ou encore dans le workspace gitpod

Un PC CNES bureautique peut etre utilisé pour les étapes ci-dessous (connexion au cluster TREX)

Les procédures pour ce projet se sont effectuées dans cet environnement:

- Python 3.7.2 (via le portail de distribution)
- VScode 1.68.1 (via le portail officiel, en version [User Installer](https://code.visualstudio.com/download#))

## Téléchargement du code

TODO: credentials + tortoise-git

```accès depuis un terminal linux
> git clone https://oauth2:montokengitlab@gitlab.com/moncompte/monprojet.git
> cd calculatrice
```

## Execution du code

Il est conseillé d'utiliser un environnement virtuel:

```shell
>  python -m venv .venv
> source ./.venv/bin/activate
> pip install -r requirements.txt .
> pip show calculatrice
> 
```

Lancer le "main": 
```python
> python src/calculatrice/calcule.py
2
```

Lancer les tests unitaires:
```python
# Lancement simple: 
# > python -m unittest tests/test_calculatrice.py
# Lancement des tests avec détermination de la couverture structurelle de code
> pytest --junitxml=unitests-report.xml --cov-report xml:./coverage-report.xml --cov=./ tests
============================================================== test session starts ===============================================================
platform linux -- Python 3.12.1, pytest-7.4.4, pluggy-1.3.0
rootdir: /workspace/calculatrice
configfile: pytest.ini
plugins: cov-4.1.0
collected 5 items                                                                                                                                

tests/test_calculatrice.py .....                                                                                                           [100%]

---------------------------------------- generated xml file: /workspace/calculatrice/unitests-report.xml -----------------------------------------

---------- coverage: platform linux, python 3.12.1-final-0 -----------
Coverage XML written to file ./coverage-report.xml

=============================================================== 5 passed in 0.19s ================================================================
```