
import calculatrice as calculatrice


def main():
    """this 'main' function' can be tested by unit test"""

    calculette = calculatrice.Calculatrice()
    resultat = calculette.additionner (1,1)
    
    print (resultat)

# __________MAIN ____________

# ":": trick to not cover the line below, see https://www.py4u.net/discuss/18677
if __name__ == "__main__":\
    main()